<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    #[Route('/test/{countryCode}', name: 'app_test_get_country', methods: ['GET'])]
    public function getCountry($countryCode = 'en_GB') : JsonResponse
    {
        if ($countryCode === 'en_GB') return $this->json([
            'country' => 'Royaume-Uni'
        ]);
        if ($countryCode === 'fr_FR') return $this->json([
            'country' => 'France'
        ]);
        return $this->json([
            'status' => JsonResponse::HTTP_NOT_FOUND, 
            'message' => 'This country does not exist yet.'
        ], JsonResponse::HTTP_NOT_FOUND);
    }
}
